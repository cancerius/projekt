
public interface StoreInterface {
	public String get(String key);
	public void put(String key, String value);
}
