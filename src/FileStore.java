import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileStore implements StoreInterface {
	private String filePath = "store/"; 
	
	public String get(String key){
		// return Files.readAllLines(Paths.get(this.filePath + key));
		try {
			return new String(Files.readAllBytes(Paths.get(this.filePath + key)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public void put(String key, String value){
		PrintStream out;
		try {
			out = new PrintStream(new FileOutputStream(this.filePath + key));
			out.println(value);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
