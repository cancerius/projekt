import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class CacheStore implements StoreInterface {
	Map<String, String> data = new HashMap<String, String>();
	
	public String get(String key){
		return this.data.getOrDefault(key, "aaaa");
	}
	
	public void put(String key, String value){
		this.data.put(key , value);
	}
}
